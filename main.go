package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"

	gcr "gitlab.com/jidckii/gc-registry/gcregistry"
)

var (
	approve         = flag.Bool("approve", false, "By default, dry run. Set true to delete tags")
	gitlabURL       = flag.String("url", "https://gitlab.com/jidckii/gc-registry", "URL to project")
	keepN           = flag.Uint("keep-n", 0, "The amount of latest tags of given name to keep.")
	manual          = flag.Bool("manual", false, "Create a list of tags and delete them yourself. Without method https://bit.ly/2UI6uHx")
	nameRegexDelete = flag.String("name-regex-delete", ".*", "The re2 regex of the name to delete. To delete all tags specify .* ")
	nameRegexKeep   = flag.String("name-regex-keep", "v.*", "The re2 regex of the name to keep. This value will override any matches from name-regex-delete. Note: setting to .* will result in a no-op.")
	olderThan       = flag.String("older-than", "1month", "Tags to delete that are older than the given time, written in human readable form 1h, 1d, 1month.")
	token           = flag.String("token", "qwerty12-", "Gitlab API token")
)

func main() {
	flag.Parse()
	config := &gcr.Config{
		NameRegexDelete: *nameRegexDelete,
		NameRegexKeep:   *nameRegexKeep,
		KeepN:           *keepN,
		OlderThan:       *olderThan,
	}
	eu := gcr.URLEscape(*gitlabURL)
	repos := gcr.GetRepos(eu+"/registry/repositories", *token)

	for _, v := range *repos {

		fmt.Println("Found docker registry: ", v.Location)
		fmt.Print("Configuration for remove: ( ")
		fmt.Print("name_regex_delete: ", config.NameRegexDelete, " | ")
		fmt.Print("name_regex_keep: ", config.NameRegexKeep, " | ")
		fmt.Print("keep_n: ", config.KeepN, " | ")
		fmt.Println("older_than: ", config.OlderThan, ")")

		URL := eu + "/registry/repositories/" + strconv.Itoa(v.ID) + "/tags"
		if *approve && !*manual {
			delbulk, err := gcr.DeleteBulk(URL, *token, config)
			if err != nil {
				log.Println(err)
			}
			fmt.Println(delbulk)
		} else if *manual {
			fmt.Println("In development")
		}
	}
}
