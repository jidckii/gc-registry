package gcregistry

import "time"

// Repos список docker репозиториев в проекте
type Repos []struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	Path      string    `json:"path"`
	ProjectID int       `json:"project_id"`
	Location  string    `json:"location"`
	CreatedAt time.Time `json:"created_at"`
}

// Config правила по которым будет выполняться удаление
type Config struct {
	NameRegexDelete string `json:"name_regex_delete"`
	NameRegexKeep   string `json:"name_regex_keep"`
	KeepN           uint   `json:"keep_n"`
	OlderThan       string `json:"older_than"`
} 