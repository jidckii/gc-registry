package gcregistry

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// DeleteBulk массовое удаление тегов в репозитории при помощи метода гитлаба:
// https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository-tags-in-bulk
// Этот метод работает только в проектах созданных в GitLab version 12.8 и старше.
func DeleteBulk(URL string, token string, config *Config) (s string, err error) {

	jsonBody, _ := json.Marshal(config)
	client := &http.Client{}
	req, err := http.NewRequest("DELETE", URL, bytes.NewBuffer(jsonBody))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("PRIVATE-TOKEN", token)
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 202 {
		return "", fmt.Errorf("%s %v %s", "HTTP code: ", resp.StatusCode, string(body))
	}
	return "Successful", nil
}
