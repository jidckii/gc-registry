package gcregistry

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// URLEscape энкодинг uri path, что бы не обращаться по ID
// https://docs.gitlab.com/ee/api/container_registry.html#list-registry-repositories
func URLEscape(refurl string) string {
	var pathuri string
	u, err := url.Parse(refurl)
	if err != nil {
		log.Fatal(err)
	}
	if string(u.Path[0]) == "/" {
		pathuri = url.QueryEscape(u.Path[1:])
	} else {
		pathuri = url.QueryEscape(u.Path)
	}
	scheme := u.Scheme
	host := u.Host
	es := (scheme + "://" + host + "/api/v4/projects/" + pathuri)
	return es
}

// GetRepos получить список репозиториев в проекте
// https://docs.gitlab.com/ee/api/container_registry.html#get-details-of-a-registry-repository-tag
func GetRepos(url string, token string) *Repos {

	// По умолчанию Gitlab отдаёт списки страницами по 20 элементов, максимум 100 элементов на 1 странице
	// https://docs.gitlab.com/ee/api/#offset-based-pagination
	perPage := 100
	page := 1
	repos := Repos{}

	for i := perPage; i == perPage; page++ {

		urlForGet := (url + "?" + "per_page=" + strconv.Itoa(perPage) + "&" + "page=" + strconv.Itoa(page))

		client := &http.Client{}
		req, err := http.NewRequest("GET", urlForGet, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("PRIVATE-TOKEN", token)

		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		respRepos := Repos{}
		err = json.Unmarshal(body, &respRepos)
		if err != nil {
			log.Fatal(err)
		}
		i = (len(respRepos))

		for _, v := range respRepos {
			repos = append(repos, v)
		}
	}

	return &repos
}
